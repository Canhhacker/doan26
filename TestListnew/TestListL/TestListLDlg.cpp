﻿
// TestListLDlg.cpp : implementation file
//

#include "stdafx.h"
#include "TestListL.h"
#include "TestListLDlg.h"
#include "afxdialogex.h"
#include "Data_Test.h"
#include "fstream"
#include "file_test.h"
#include "string"


using namespace std;
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CTestListLDlg dialog



CTestListLDlg::CTestListLDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_TESTLISTL_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTestListLDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDT_HODEM, edit_hodem);
	DDX_Control(pDX, IDC_EDT_TEN, edit_ten);
	DDX_Control(pDX, IDC_EDT_GIOITINH, edit_gioitinh);
	DDX_Control(pDX, IDC_EDT_QUEQUAN, edit_quequan);
	DDX_Control(pDX, IDC_EDT_TRINHDO, edit_trinhdo);
	DDX_Control(pDX, IDC_EDT_CHUCVU, edit_chucvu);
	DDX_Control(pDX, IDC_CB_NGAYSINH, cb_ngaysinh);
	DDX_Control(pDX, IDC_CB_THANGSINH, cb_thangsinh);
	DDX_Control(pDX, IDC_CB_NAMSINH, cb_namsinh);
	DDX_Control(pDX, IDC_CB_NGAYCT, cb_ngayct);
	DDX_Control(pDX, IDC_CB_THANGCT, cb_thangct);
	DDX_Control(pDX, IDC_CB_NAMCT, cb_namct);
	DDX_Control(pDX, IDC_CB_HOPDONG, cb_hopdong);
	DDX_Control(pDX, IDC_EDT_MAKHOA, edit_makhoa);
	DDX_Control(pDX, IDC_EDT_TENKHOA, edit_tenkhoa);
	DDX_Control(pDX, IDC_EDT_MALUONG, edit_maluong);
	DDX_Control(pDX, IDC_EDT_BACLUONG, edit_bacluong);
	DDX_Control(pDX, IDC_EDT_HESOLUONG, edit_hesoluong);
	DDX_Control(pDX, IDC_EDT_LUONGCOBAN, edit_luongcoban);
	DDX_Control(pDX, IDC_LIST_INFOR, list_control_infor);
}

BEGIN_MESSAGE_MAP(CTestListLDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BNT_ADD, &CTestListLDlg::OnBnClickedBntAdd)
END_MESSAGE_MAP()


// CTestListLDlg message handlers

BOOL CTestListLDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	cb_ngaysinh.AddString(_T("01"));
	cb_ngaysinh.AddString(_T("02"));
	cb_ngaysinh.AddString(_T("03"));
	cb_ngaysinh.AddString(_T("04"));
	cb_ngaysinh.AddString(_T("05"));
	cb_ngaysinh.AddString(_T("06"));
	cb_ngaysinh.AddString(_T("07"));
	cb_ngaysinh.AddString(_T("08"));
	cb_ngaysinh.AddString(_T("09"));
	cb_ngaysinh.AddString(_T("10"));
	cb_ngaysinh.AddString(_T("11"));
	cb_ngaysinh.AddString(_T("12"));
	cb_ngaysinh.AddString(_T("13"));
	cb_ngaysinh.AddString(_T("14"));
	cb_ngaysinh.AddString(_T("15"));
	cb_ngaysinh.AddString(_T("16"));
	cb_ngaysinh.AddString(_T("17"));
	cb_ngaysinh.AddString(_T("18"));
	cb_ngaysinh.AddString(_T("19"));
	cb_ngaysinh.AddString(_T("20"));
	cb_ngaysinh.AddString(_T("21"));
	cb_ngaysinh.AddString(_T("22"));
	cb_ngaysinh.AddString(_T("23"));
	cb_ngaysinh.AddString(_T("24"));
	cb_ngaysinh.AddString(_T("25"));
	cb_ngaysinh.AddString(_T("26"));
	cb_ngaysinh.AddString(_T("27"));
	cb_ngaysinh.AddString(_T("28"));
	cb_ngaysinh.AddString(_T("29"));
	cb_ngaysinh.AddString(_T("30"));
	cb_ngaysinh.SetCurSel(0);
	cb_ngaysinh.AddString(_T("31"));
	cb_thangsinh.AddString(_T("01"));
	cb_thangsinh.AddString(_T("02"));
	cb_thangsinh.AddString(_T("03"));
	cb_thangsinh.AddString(_T("04"));
	cb_thangsinh.AddString(_T("05"));
	cb_thangsinh.AddString(_T("06"));
	cb_thangsinh.AddString(_T("07"));
	cb_thangsinh.AddString(_T("08"));
	cb_thangsinh.AddString(_T("09"));
	cb_thangsinh.AddString(_T("10"));
	cb_thangsinh.AddString(_T("11"));
	cb_thangsinh.AddString(_T("12"));
	cb_thangsinh.SetCurSel(0);
	cb_namsinh.AddString(_T("1970"));
	cb_namsinh.AddString(_T("1971"));
	cb_namsinh.AddString(_T("1972"));
	cb_namsinh.AddString(_T("1973"));
	cb_namsinh.AddString(_T("1974"));
	cb_namsinh.AddString(_T("1975"));
	cb_namsinh.AddString(_T("1976"));
	cb_namsinh.AddString(_T("1977"));
	cb_namsinh.AddString(_T("1978"));
	cb_namsinh.AddString(_T("1979"));
	cb_namsinh.AddString(_T("1980"));
	cb_namsinh.AddString(_T("1981"));
	cb_namsinh.AddString(_T("1982"));
	cb_namsinh.AddString(_T("1983"));
	cb_namsinh.AddString(_T("1984"));
	cb_namsinh.AddString(_T("1985"));
	cb_namsinh.AddString(_T("1986"));
	cb_namsinh.AddString(_T("1987"));
	cb_namsinh.AddString(_T("1988"));
	cb_namsinh.AddString(_T("1989"));
	cb_namsinh.AddString(_T("1990"));
	cb_namsinh.AddString(_T("1991"));
	cb_namsinh.AddString(_T("1992"));
	cb_namsinh.AddString(_T("1993"));
	cb_namsinh.AddString(_T("1994"));
	cb_namsinh.AddString(_T("1995"));
	cb_namsinh.AddString(_T("1996"));
	cb_namsinh.AddString(_T("1997"));
	cb_namsinh.AddString(_T("1998"));
	cb_namsinh.AddString(_T("1999"));
	cb_namsinh.AddString(_T("2000"));
	cb_namsinh.SetCurSel(0);


	cb_ngayct.AddString(_T("01"));
	cb_ngayct.AddString(_T("02"));
	cb_ngayct.AddString(_T("03"));
	cb_ngayct.AddString(_T("04"));
	cb_ngayct.AddString(_T("05"));
	cb_ngayct.AddString(_T("06"));
	cb_ngayct.AddString(_T("07"));
	cb_ngayct.AddString(_T("08"));
	cb_ngayct.AddString(_T("09"));
	cb_ngayct.AddString(_T("10"));
	cb_ngayct.AddString(_T("11"));
	cb_ngayct.AddString(_T("12"));
	cb_ngayct.AddString(_T("13"));
	cb_ngayct.AddString(_T("14"));
	cb_ngayct.AddString(_T("15"));
	cb_ngayct.AddString(_T("16"));
	cb_ngayct.AddString(_T("17"));
	cb_ngayct.AddString(_T("18"));
	cb_ngayct.AddString(_T("19"));
	cb_ngayct.AddString(_T("20"));
	cb_ngayct.AddString(_T("21"));
	cb_ngayct.AddString(_T("22"));
	cb_ngayct.AddString(_T("23"));
	cb_ngayct.AddString(_T("24"));
	cb_ngayct.AddString(_T("25"));
	cb_ngayct.AddString(_T("26"));
	cb_ngayct.AddString(_T("27"));
	cb_ngayct.AddString(_T("28"));
	cb_ngayct.AddString(_T("29"));
	cb_ngayct.AddString(_T("30"));
	cb_ngayct.AddString(_T("31"));
	cb_ngayct.SetCurSel(0);
	cb_thangct.AddString(_T("01"));
	cb_thangct.AddString(_T("01"));
	cb_thangct.AddString(_T("02"));
	cb_thangct.AddString(_T("03"));
	cb_thangct.AddString(_T("04"));
	cb_thangct.AddString(_T("05"));
	cb_thangct.AddString(_T("06"));
	cb_thangct.AddString(_T("07"));
	cb_thangct.AddString(_T("08"));
	cb_thangct.AddString(_T("09"));
	cb_thangct.AddString(_T("10"));
	cb_thangct.AddString(_T("11"));
	cb_thangct.AddString(_T("12"));
	cb_thangct.SetCurSel(0);
	cb_namct.AddString(_T("1980"));
	cb_namct.AddString(_T("1981"));
	cb_namct.AddString(_T("1982"));
	cb_namct.AddString(_T("1983"));
	cb_namct.AddString(_T("1984"));
	cb_namct.AddString(_T("1985"));
	cb_namct.AddString(_T("1986"));
	cb_namct.AddString(_T("1987"));
	cb_namct.AddString(_T("1988"));
	cb_namct.AddString(_T("1989"));
	cb_namct.AddString(_T("1990"));
	cb_namct.AddString(_T("1991"));
	cb_namct.AddString(_T("1992"));
	cb_namct.AddString(_T("1993"));
	cb_namct.AddString(_T("1994"));
	cb_namct.AddString(_T("1995"));
	cb_namct.AddString(_T("1996"));
	cb_namct.AddString(_T("1997"));
	cb_namct.AddString(_T("1998"));
	cb_namct.AddString(_T("1999"));
	cb_namct.AddString(_T("2000"));
	cb_namct.AddString(_T("2001"));
	cb_namct.AddString(_T("2002"));
	cb_namct.AddString(_T("2003"));
	cb_namct.AddString(_T("2004"));
	cb_namct.AddString(_T("2005"));
	cb_namct.AddString(_T("2007"));
	cb_namct.AddString(_T("2008"));
	cb_namct.AddString(_T("2009"));
	cb_namct.AddString(_T("2010"));
	cb_namct.AddString(_T("2011"));
	cb_namct.AddString(_T("2012"));
	cb_namct.AddString(_T("2013"));
	cb_namct.AddString(_T("2014"));
	cb_namct.AddString(_T("2015"));
	cb_namct.AddString(_T("2016"));
	cb_namct.AddString(_T("2017"));
	cb_namct.AddString(_T("2018"));
	cb_namct.AddString(_T("2019"));
	cb_namct.AddString(_T("2020"));
	cb_namct.SetCurSel(0);

	cb_hopdong.AddString(_T("Hợp đồng"));
	cb_hopdong.AddString(_T("Biên chế"));
	cb_hopdong.SetCurSel(0);

	list_control_infor.SetExtendedStyle(LVS_EX_FULLROWSELECT || LVS_EX_GRIDLINES);
	list_control_infor.InsertColumn(0, _T("Họ đệm"), LVCFMT_LEFT, 100);
	list_control_infor.InsertColumn(1, _T("Tên"), LVCFMT_LEFT, 50);
	list_control_infor.InsertColumn(2, _T("Giới tính"), LVCFMT_LEFT, 50);
	list_control_infor.InsertColumn(3, _T("Quê quán"), LVCFMT_LEFT, 100);
	list_control_infor.InsertColumn(4, _T("Trình độ"), LVCFMT_LEFT, 50);
	list_control_infor.InsertColumn(5, _T("Chức vụ"), LVCFMT_LEFT, 50);
	list_control_infor.InsertColumn(6, _T("Ngày sinh"), LVCFMT_LEFT, 100);
	list_control_infor.InsertColumn(7, _T("Ngày công tác"), LVCFMT_LEFT,100);
	list_control_infor.InsertColumn(8, _T("Hợp đồng"), LVCFMT_LEFT, 50);
	list_control_infor.InsertColumn(9, _T("Mã khoa"), LVCFMT_LEFT, 50);
	list_control_infor.InsertColumn(10, _T("Tên khoa"), LVCFMT_LEFT, 100);
	list_control_infor.InsertColumn(11, _T("Mã lương"), LVCFMT_LEFT, 100);
	list_control_infor.InsertColumn(12, _T("Bậc lương"), LVCFMT_LEFT,100);
	list_control_infor.InsertColumn(13, _T("Hệ số lương"), LVCFMT_LEFT, 100);
	list_control_infor.InsertColumn(14, _T("Lương cơ bản"), LVCFMT_LEFT, 100);
	
	File file;
	NV n_v;
	ListNV l;
	n_v.KhoiTaoList(l);
	file.Read_List_NV(l);
	for (NodeNhanVien* i = l.pHead; i != NULL; i = i->pNext)
	{
		string str_mavn = i->Data.MaNhanVien;
		string str_hodem = i->Data.Ho_Dem;
		string str_ten = i->Data.Ten;
		string str_gioitinh = i->Data.GioiTinh;
		string str_quequan = i->Data.QueQuan;
		string str_ngaysinh = to_string(i->Data.NgaySinh.Ngay) + "/" + to_string(i->Data.NgaySinh.Thang) + "/" + to_string(i->Data.NgaySinh.Nam);
		string str_ngaycongtac = to_string(i->Data.NgayBatDauCongTac.Ngay) + "/" + to_string(i->Data.NgayBatDauCongTac.Thang) + "/" + to_string(i->Data.NgayBatDauCongTac.Nam);
		string str_vienchuc = i->Data.VienChuc;
		string str_trinhdo = i->Data.TrinhDoChuyenMon;
		string str_chucvu = i->Data.ChucVu;
		string str_makhoa = i->Data.KhoaNV.MaKhoa;
		string str_tenkhoa = i->Data.KhoaNV.TenKhoa;
		string str_maluong = i->Data.LuongNV.MaLuong;
		string str_bacluong = to_string(i->Data.LuongNV.BacLuong);
		string str_hesoluong = to_string(i->Data.LuongNV.HeSoLuong);
		string str_luongcoban = to_string(i->Data.LuongNV.LuongCoBan);
		CString cstr_hodem(str_hodem.c_str(), str_hodem.length());
		CString cstr_ten(str_ten.c_str(), str_ten.length());
		CString cstr_gioitinh(str_gioitinh.c_str(), str_gioitinh.length());
		CString cstr_quequan(str_quequan.c_str(), str_quequan.length());
		CString cstr_ngaysinh(str_ngaysinh.c_str(), str_ngaysinh.length());
		CString cstr_ngaycongtac(str_ngaycongtac.c_str(), str_ngaycongtac.length());
		CString cstr_vienchuc(str_vienchuc.c_str(), str_vienchuc.length());
		CString cstr_trinhdo(str_trinhdo.c_str(), str_trinhdo.length());
		CString cstr_makhoa(str_makhoa.c_str(), str_makhoa.length());
		CString cstr_tenkhoa(str_tenkhoa.c_str(), str_tenkhoa.length());
		CString cstr_chucvu(str_chucvu.c_str(), str_chucvu.length());
		CString cstr_maluong(str_maluong.c_str(), str_maluong.length());
		CString cstr_bacluong(str_bacluong.c_str(), str_bacluong.length());
		CString cstr_hesoluong(str_hesoluong.c_str(), str_hesoluong.length());
		CString cstr_luongcoban(str_luongcoban.c_str(), str_luongcoban.length());

		list_control_infor.InsertItem(0, cstr_hodem);
		list_control_infor.SetItemText(0, 1, cstr_ten);
		list_control_infor.SetItemText(0, 2, cstr_gioitinh);
		list_control_infor.SetItemText(0, 3, cstr_quequan);
		list_control_infor.SetItemText(0, 4, cstr_trinhdo);
		list_control_infor.SetItemText(0, 5, cstr_chucvu);
		list_control_infor.SetItemText(0, 8, cstr_vienchuc);
		list_control_infor.SetItemText(0, 9, cstr_makhoa);
		list_control_infor.SetItemText(0, 10, cstr_tenkhoa);
		list_control_infor.SetItemText(0, 11, cstr_maluong);
		list_control_infor.SetItemText(0, 12, cstr_bacluong);
		list_control_infor.SetItemText(0, 13, cstr_hesoluong);
		list_control_infor.SetItemText(0, 14, cstr_luongcoban);
		list_control_infor.SetItemText(0, 6, cstr_ngaysinh);
		list_control_infor.SetItemText(0, 7, cstr_ngaycongtac);
	}

	UpdateData(false);

	return TRUE;

}

void CTestListLDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CTestListLDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CTestListLDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTestListLDlg::OnBnClickedBntAdd()
{
	UpdateData(true);
	CString edt_hodem;
	edit_hodem.GetWindowText(edt_hodem);
	CString edt_ten;
	edit_ten.GetWindowText(edt_ten);
	CString edt_gioitinh;
	edit_gioitinh.GetWindowText(edt_gioitinh);
	CString edt_quequan;
	edit_quequan.GetWindowText(edt_quequan);
	CString edt_trinhdo;
	edit_trinhdo.GetWindowText(edt_trinhdo);
	CString edt_chucvu;
	edit_chucvu.GetWindowText(edt_chucvu);
	CString edt_makhoa;
	edit_makhoa.GetWindowText(edt_makhoa);
	CString edt_tenkhoa;
	edit_tenkhoa.GetWindowText(edt_tenkhoa);
	CString edt_maluong;
	edit_maluong.GetWindowText(edt_maluong);
	CString edt_bacluong;
	edit_bacluong.GetWindowText(edt_bacluong);
	CString edt_hesoluong;
	edit_hesoluong.GetWindowText(edt_hesoluong);
	CString edt_luongcoban;
	edit_luongcoban.GetWindowText(edt_luongcoban);
	CString str_hopdong;
	int ihd = cb_hopdong.GetCurSel();
	cb_hopdong.GetLBText(ihd, str_hopdong);

	CString str_ngaysinh;
	int ins = cb_ngaysinh.GetCurSel();
	cb_ngaysinh.GetLBText(ins, str_ngaysinh);
	CString str_thangsinh;
	int its = cb_thangsinh.GetCurSel();
	cb_thangsinh.GetLBText(its, str_thangsinh);
	CString str_namsinh;
	int inas = cb_namsinh.GetCurSel();
	cb_namsinh.GetLBText(inas, str_namsinh);

	string srt_ngaysinh = CStringA(str_ngaysinh) + "/" + CStringA(str_thangsinh) + "/" + CStringA(str_namsinh);
	CString csrt_ngaysinh(srt_ngaysinh.c_str(), srt_ngaysinh.length());

	CString str_ngayct;
	int inct = cb_ngayct.GetCurSel();
	cb_ngayct.GetLBText(inct, str_ngayct);
	CString str_thangct;
	int itct = cb_thangct.GetCurSel();
	cb_thangct.GetLBText(itct, str_thangct);
	CString str_namct;
	int inact = cb_namct.GetCurSel();
	cb_namct.GetLBText(inact, str_namct);

	string str_ngaycongtac = CStringA(str_ngayct) + "/" + CStringA(str_thangct) + "/" + CStringA(str_namct);
	CString cstr_ngaycongtac(str_ngaycongtac.c_str(), str_ngaycongtac.length());

	CString str_infor;
	list_control_infor.InsertItem(0, edt_hodem);
	list_control_infor.SetItemText(0, 1, edt_ten);
	list_control_infor.SetItemText(0, 2, edt_gioitinh);
	list_control_infor.SetItemText(0, 3, edt_quequan);
	list_control_infor.SetItemText(0, 4, edt_trinhdo);
	list_control_infor.SetItemText(0, 5, edt_chucvu);
	list_control_infor.SetItemText(0, 8, str_hopdong);
	list_control_infor.SetItemText(0, 9, edt_makhoa);
	list_control_infor.SetItemText(0, 10, edt_tenkhoa);
	list_control_infor.SetItemText(0, 11, edt_maluong);
	list_control_infor.SetItemText(0, 12, edt_bacluong);
	list_control_infor.SetItemText(0, 13, edt_hesoluong);
	list_control_infor.SetItemText(0, 14, edt_luongcoban);
	list_control_infor.SetItemText(0, 6, csrt_ngaysinh);
	list_control_infor.SetItemText(0, 7, cstr_ngaycongtac);
	UpdateData(false);
}
