﻿#pragma once
#include <string>
using namespace std;

struct Luong {
	string MaLuong;
	float BacLuong;
	float HeSoLuong;
	float LuongCoBan;
};

struct NodeLuong {
	Luong Data;
	NodeLuong* pNext;
};

struct ListL {
	NodeLuong* pHead;
	NodeLuong* pTail;
};

struct NgayThang {
	int Ngay;
	int Thang;
	int Nam;
};
struct Khoa {
	string MaKhoa;
	string TenKhoa;

};

struct NhanVien
{
	string MaNhanVien;
	string Ten;
	string Ho_Dem;
	string GioiTinh;
	string QueQuan;
	NgayThang NgaySinh;
	string TrinhDoChuyenMon;
	string ChucVu;
	NgayThang NgayBatDauCongTac;
	string VienChuc;
	Luong LuongNV;
	Khoa KhoaNV;
};
struct NodeNhanVien {
	NhanVien Data;
	NodeNhanVien* pNext;
};

struct ListNV {
	NodeNhanVien* pHead;
	NodeNhanVien* pTail;
};



class NV
{
private:
	NhanVien nv;
	ListNV l;
public:
	NodeNhanVien* KhoiTaoNode(NhanVien x);
	void ThemCuoi(ListNV& l, NodeNhanVien* x);
	void KhoiTaoList(ListNV& l);
};


