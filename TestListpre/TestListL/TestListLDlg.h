
// TestListLDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CTestListLDlg dialog
class CTestListLDlg : public CDialogEx
{
// Construction
public:
	CTestListLDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TESTLISTL_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CEdit edit_hodem;
	CEdit edit_ten;
	CEdit edit_gioitinh;
	CEdit edit_quequan;
	CEdit edit_trinhdo;
	CEdit edit_chucvu;
	CComboBox cb_ngaysinh;
	CComboBox cb_thangsinh;
	CComboBox cb_namsinh;
	CComboBox cb_ngayct;
	CComboBox cb_thangct;
	CComboBox cb_namct;
	CComboBox cb_hopdong;
	CEdit edit_makhoa;
	CEdit edit_tenkhoa;
	CEdit edit_maluong;
	CEdit edit_bacluong;
	CEdit edit_hesoluong;
	CEdit edit_luongcoban;
	CListCtrl list_control_infor;
	afx_msg void OnBnClickedBntAdd();
	
};
