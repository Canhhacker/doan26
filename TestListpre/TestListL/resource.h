//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TestListL.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TESTLISTL_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDC_BNT_ADD                     1000
#define IDC_EDT_HODEM                   1001
#define IDC_RADIO1                      1002
#define IDC_EDT_GIOITINH                1004
#define IDC_LIST_INFOR                  1006
#define IDC_EDT_TRINHDO                 1007
#define IDC_EDT_CHUCVU                  1008
#define IDC_CB_NAMSINH                  1011
#define IDC_CB_HOPDONG                  1012
#define IDC_EDT_MALUONG                 1017
#define IDC_EDT_BACLUONG                1018
#define IDC_CB_THANGSINH                1019
#define IDC_CB_NGAYSINH                 1020
#define IDC_EDT_QUEQUAN                 1021
#define IDC_EDT_TEN                     1024
#define IDC_EDT_HESOLUONG               1025
#define IDC_EDT_LUONGCOBAN              1026
#define IDC_CB_NAMCT                    1030
#define IDC_CB_THANGCT                  1031
#define IDC_CB_NGAYCT                   1032
#define IDC_EDT_TENKHOA                 1036
#define IDC_EDT_MAKHOA                  1037

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1003
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
