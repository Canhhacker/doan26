#pragma once


// Dlg_admin_right dialog

class Dlg_admin_right : public CDialogEx
{
	DECLARE_DYNAMIC(Dlg_admin_right)

public:
	Dlg_admin_right(CWnd* pParent = nullptr);   // standard constructor
	virtual ~Dlg_admin_right();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_ADMIN_RIGHT };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio1();
protected:
	CButton rad_nhap_nhan_vien;
	CButton rad_xuat;
	CButton rad_sua;
	CButton rad_xoa;
	CButton rad_tim_kiem;
};
