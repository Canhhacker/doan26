//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MFCTestDoAn.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MFC_TESTDOAN_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_NOTIFY               130
#define IDD_DIALOG_ADMIN_RIGHT          132
#define IDD_DIALOG_NORMAL_RIGHT         134
#define IDC_RADIO_QUANLY                1000
#define IDC_RADIO_NHANVIEN              1001
#define IDC_BUTTON_LOGIN                1002
#define IDC_EDIT_PASSWORD               1003
#define IDC_BUTTON_LOGIN_PASSWORD       1004
#define IDC_RADIO_NHAP_NHAN_VIEN		1005
#define IDC_RADIO_XUAT					1006
#define IDC_RADIO_SUA					1007
#define IDC_RADIO_XOA					1008
#define IDC_RADIO_TIM_KIEM				1009
#define IDC_RADIO_XUAT_2				1010
#define IDC_RADIO_TIM_KIEM_2			1011
#define IDC_BUTTON_OK_ADMIN_RIGHT		1013
#define IDC_BUTTON_OK_NORMAL_RIGHT		1014


// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
