#pragma once


// Dlg_Login_Notify dialog

class Dlg_Login_Notify : public CDialogEx
{
	DECLARE_DYNAMIC(Dlg_Login_Notify)

public:
	Dlg_Login_Notify(CWnd* pParent = nullptr);   // standard constructor
	virtual ~Dlg_Login_Notify();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_NOTIFY };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonLoginPassword();
protected:
	CEdit edit_password_;
};
