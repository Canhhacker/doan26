// Dlg_normal_right.cpp : implementation file
//

#include "pch.h"
#include "MFC_TestDoAn.h"
#include "Dlg_normal_right.h"
#include "afxdialogex.h"


// Dlg_normal_right dialog

IMPLEMENT_DYNAMIC(Dlg_normal_right, CDialogEx)

Dlg_normal_right::Dlg_normal_right(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_NORMAL_RIGHT, pParent)
{

}

Dlg_normal_right::~Dlg_normal_right()
{
}

void Dlg_normal_right::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Dlg_normal_right, CDialogEx)
END_MESSAGE_MAP()


// Dlg_normal_right message handlers
