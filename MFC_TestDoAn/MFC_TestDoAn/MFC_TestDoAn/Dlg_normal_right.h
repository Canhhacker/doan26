#pragma once


// Dlg_normal_right dialog

class Dlg_normal_right : public CDialogEx
{
	DECLARE_DYNAMIC(Dlg_normal_right)

public:
	Dlg_normal_right(CWnd* pParent = nullptr);   // standard constructor
	virtual ~Dlg_normal_right();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG_NORMAL_RIGHT };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
