// Dlg_admin_right.cpp : implementation file
//

#include "pch.h"
#include "MFC_TestDoAn.h"
#include "Dlg_admin_right.h"
#include "afxdialogex.h"



// Dlg_admin_right dialog

IMPLEMENT_DYNAMIC(Dlg_admin_right, CDialogEx)

Dlg_admin_right::Dlg_admin_right(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_ADMIN_RIGHT, pParent)
{

}

Dlg_admin_right::~Dlg_admin_right()
{
}

void Dlg_admin_right::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	
	DDX_Check(pDX, IDC_RADIO_NHAP_NHAN_VIEN, (int&)rad_nhap_nhan_vien);
	DDX_Check(pDX, IDC_RADIO_XUAT, (int&)rad_xuat);
	DDX_Check(pDX, IDC_RADIO_SUA, (int&)rad_sua);
	DDX_Check(pDX, IDC_RADIO_XOA, (int&)rad_xoa);
	DDX_Check(pDX, IDC_RADIO_TIM_KIEM, (int&)rad_tim_kiem);
}


BEGIN_MESSAGE_MAP(Dlg_admin_right, CDialogEx)
END_MESSAGE_MAP()


// Dlg_admin_right message handlers
