
// MFC_TestDoAn.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CMFCTestDoAnApp:
// See MFC_TestDoAn.cpp for the implementation of this class
//

class CMFCTestDoAnApp : public CWinApp
{
public:
	CMFCTestDoAnApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CMFCTestDoAnApp theApp;
