// Dlg_Login_Notify.cpp : implementation file
//

#include "pch.h"
#include "MFC_TestDoAn.h"
#include "Dlg_Login_Notify.h"
#include "afxdialogex.h"
#include "Dlg_admin_right.h"

// Dlg_Login_Notify dialog

IMPLEMENT_DYNAMIC(Dlg_Login_Notify, CDialogEx)

Dlg_Login_Notify::Dlg_Login_Notify(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG_NOTIFY, pParent)
{

}

Dlg_Login_Notify::~Dlg_Login_Notify()
{
}

void Dlg_Login_Notify::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_PASSWORD, edit_password_);
}


BEGIN_MESSAGE_MAP(Dlg_Login_Notify, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON_LOGIN_PASSWORD, &Dlg_Login_Notify::OnBnClickedButtonLoginPassword)
END_MESSAGE_MAP()


// Dlg_Login_Notify message handlers


void Dlg_Login_Notify::OnBnClickedButtonLoginPassword()
{
	CString str_pass = _T("123456");

	CString input_pass;

	edit_password_.GetWindowText(input_pass);

	bool is_login = false;

	if (input_pass.Compare(str_pass) == 0) {
		is_login = true;
	}

	if (is_login) {
		MessageBox(_T("Login Success !"), _T("Info"), MB_OK | MB_ICONINFORMATION);
		Dlg_admin_right dlg_admin_right;
		dlg_admin_right.DoModal();
	}
	else {
		MessageBox(_T("Login Failed !"), _T("Info"), MB_OK | MB_ICONERROR);
	}
}
