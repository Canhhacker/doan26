#include "pch.h"
#include "Data_Test.h"

void NV::KhoiTaoList(ListNV &l) {
	l.pHead = l.pTail = NULL;
}
NodeNhanVien* NV::KhoiTaoNode(NhanVien x) {
	NodeNhanVien *p = new NodeNhanVien;
	p->Data = x;
	p->pNext = NULL;
	return p;
}

void NV::ThemCuoi(ListNV &l, NodeNhanVien *x) {
	if (l.pHead == NULL)
	{
		l.pHead = l.pTail = x;
	}
	else
	{
		l.pTail->pNext = x;
		l.pTail = x;

	}
}

