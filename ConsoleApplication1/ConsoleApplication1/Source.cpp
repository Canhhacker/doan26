﻿#include<iostream>
#include<string>
#include<string.h>
using namespace std;

struct Khoa {
	string Makhoa;
	string Tenkhoa;
};

struct ListKhoa {
	Khoa Data;
	ListKhoa* pNext;
};


struct LIST
{
	ListKhoa *pHead;
	ListKhoa *pTail;
};

void KhoiTaoDSLKk(LIST &l)
{
	l.pHead = NULL;
	l.pTail = NULL;
}

void Nhap_1_Khoa(Khoa& k)
{
	while (getchar() != '\n');
	cout << "\nNhap ma khoa: ";
	getline(cin, k.Makhoa);
	cout << "\nNhap ten khoa: ";
	getline(cin, k.Tenkhoa);
}

void Xuat1Khoa(Khoa k) {
	cout << k.Makhoa << "  ";
	cout << k.Tenkhoa << "  ";
}

void Xuat_1_List_Khoa(LIST l) {
	for (ListKhoa *k = l.pHead; k != NULL; k = k->pNext)
	{
		Xuat1Khoa(k->Data);
		cout << endl;
	}
}

ListKhoa* KhoiTaoKhoa(Khoa x)
{
	ListKhoa *p = new ListKhoa;
	p->Data = x;
	p->pNext = NULL;
	return p;
}

void ThemVaoCuoiK(LIST &l,ListKhoa *p)
{
	if (l.pHead == NULL)
	{
		l.pHead = l.pTail = p;
	}
	else
	{
		l.pTail->pNext = p;
		l.pTail = p;
	}
}

struct Sinhvien {
	string Name;
	string masv;
	Khoa Makhoa;
};

struct ListSinhvien {
	Sinhvien Data;
	ListSinhvien* pNext;
};

struct list
{
	ListSinhvien *pHead;
	ListSinhvien *pTail;
};

void KhoiTaoDSLKSV(list &l)
{
	l.pHead = NULL;
	l.pTail = NULL;
}

void Nhap_1_Sinh_Vien(Sinhvien& sv)
{

	while (getchar() != '\n');
	int flag = 1;
	do
	{
		cout << "\nNhap ma sinh vien: ";
		getline(cin, sv.masv);
		flag = 1;
		for (int i = 0; i < sv.masv.length(); i++)
		{
			if (sv.masv[i] < '0' || sv.masv[i]>'9')
			{
				cout << "\nMa sinh vien phai la so!\n";
				flag = 0;
				break;
			}
		}

	} while (flag == 0);
	cout << "\nNhap ten sinh vien: ";
	getline(cin, sv.Name);
	cout << "\nNhap ma khoa sinh vien: ";
	getline(cin, sv.Makhoa.Makhoa);
}

void Xuat_1_Sinh_Vien(Sinhvien sv) {
	cout << sv.masv << "   ";
	cout << sv.Name << "   ";
	cout << sv.Makhoa.Makhoa << "   ";
	cout << endl;
}

bool XetDK(Khoa a, Khoa b) {
	if (a.Makhoa == b.Makhoa) {
		return false;
	}
	return true;
}

void XuatListSV(list lsv) {
	for (ListSinhvien* i = lsv.pHead; i->pNext != NULL; i = i->pNext) {
		Xuat_1_Sinh_Vien(i->Data);
		cout << endl;
	}
}

ListSinhvien* KhoiTaoSinhvien(Sinhvien x)
{
	ListSinhvien *p = new ListSinhvien;
	p->Data = x;
	p->pNext = NULL;
	return p;
}

void ThemVaoCuoiSV(list &l,ListSinhvien *p)
{
	if (l.pHead == NULL)
	{
		l.pHead = l.pTail = p;
	}
	else
	{
		l.pTail->pNext = p;
		l.pTail = p;
	}
}

string Catten(string& Name) {
	string x;
	for (int i = Name.length() - 1; i = 0; i--) {
		if (Name[i] == ' ') {
			x = Name.substr(i + 1);
			return x;
		}
	}
}

//string CatTen(string a){
//	string tmp = "";
//	for (int i = a.length() - 1; i >= 0; i--) {
//		if (a[i] == ' ')
//		{
//			for (int j = i + 1; j < a.length(); j++)
//			{
//				tmp.push_back(a[j]);
//			}
//			break;
//		}
//	}
//	if (tmp == "")
//		return a;
//	return tmp;
//}


void DoiCho(Sinhvien& a, Sinhvien& b) {
	Sinhvien temp = a;
	a = b;
	b = temp;
}

//void SapXepTenSinhVien(list& l,string Mak) {
//	ListSinhvien* i,* j;
//	ListSinhvien* min = l.pHead;
//	string str_min;
//	string str_tam;
//	if (l.pHead == NULL)
//	{
//		cout << "\nChua co thong tin sinh vien.";
//		system("pause");
//	}
//	else {
//		for (i = l.pHead; i->pNext != NULL; i = i->pNext) {
//			if (i->Data.Makhoa.Makhoa == Mak) {
//				min = i;
//				str_min = Catten(min->Data.Name);
//				for (j = i->pNext; j != NULL; j = j->pNext) {
//					if (j->Data.Makhoa.Makhoa == Mak)
//					{
//						str_tam = Catten(j->Data.Name);
//						if (strcmp(str_min.c_str(), str_tam.c_str()) == 1)
//						{
//							min = j;
//							str_min = str_tam;
//						}
//					}
//				}
//			}
//			DoiCho(min->Data,j->Data);
//		}
//		for (i = l.pHead; i != l.pTail; i = i->pNext)
//		{
//			if (i->Data.Makhoa.Makhoa == Mak)
//			{
//				Xuat_1_Sinh_Vien(i->Data);
//			}
//		}
//	}
//}

void sapxepten(list l,string Mak)
{
	int i = 1;
	for (ListSinhvien* i = l.pHead; i->pNext != NULL; i = i->pNext)
	{
		for (ListSinhvien* g = l.pHead; g != NULL; g = g->pNext)
		{
			string ten = Catten(i->Data.Name);
			string ten1 = Catten(g->Data.Name);
			if (_stricmp((char*)ten.c_str(), (char*)ten1.c_str()) < 0)
			{
				DoiCho(i->Data, g->Data);
			}

		}
	}
	for (ListSinhvien*i = l.pHead; i != l.pTail; i = i->pNext)
	{
		if (i->Data.Makhoa.Makhoa == Mak)
		{
				Xuat_1_Sinh_Vien(i->Data);
		}
	}
}

bool XetDK(Sinhvien a, Sinhvien b) {
	if (a.masv == b.masv) {
		return false;
	}
	return true;
}

void Menu(LIST &l,list &Lsv)
{
	int luachon;
	while (true)	
	{
		system("cls");
		cout << "\n\n\t\t========== Menu ==========";
		cout << "\n\t1. NHAP VAO DANH SACH KHOA";
		cout << "\n\t2. XUAT RA DANH SACH KHOA";
		cout << "\n\t3. NHAP VAO DANH SACH SINH VIEN";
		cout << "\n\t4. XUAT RA DANH SACH SINH VIEN";
		cout << "\n\t5. TIM NHUNG NHAN VIEN THUOC CUNG 1 KHOA DO NGUOI DUNG NHAP VAO";
		cout << "\n\t6. SAP XEP SINH VIEN THEO THU TU TANG DAN CUA KHOA";
		cout << "\n\t0. Thoat";
		cout << "\n\n\t\t========== End ===========";

		do {
			cout << "\n\t NHAP VAO LUA CHON CUA BAN: ";
			cin >> luachon;
			if (luachon < 0 || luachon >6)
			{
				cout << "\n LUA CHON CUA BAN SAI!!!! VUI LONG NHAP LAI. ";
			}
		} while (luachon < 0 || luachon >6);
		if (luachon == 1)
		{
			Khoa k;
			cout << "\nNhap khoa muon them vao: ";
			Nhap_1_Khoa(k);
			bool xet = 0;
			for (ListKhoa *i = l.pHead; i != NULL; i = i->pNext) {
				if (XetDK(k, i->Data) ==  false) {
					xet = true;
					cout << "\t\t\tMA BAN NHAP BI TRUNG NHA!!! NHAP LAI DI :)"<<endl;
					system("pause");
					break;
				}
			}
			if (xet == 0) {
				ListKhoa* lk = KhoiTaoKhoa(k);
				ThemVaoCuoiK(l, lk);
			}		
		}
		else if (luachon == 2) {
			cout << "========== Danh sach Khoa ===========" << endl;
			Xuat_1_List_Khoa(l);
			system("pause");
		}

		else if (luachon == 3)
		{
			Sinhvien sv;
			cout << "\nNhap sinhh vien muong them vao: ";
			Nhap_1_Sinh_Vien(sv);
			bool xet = 0;
			for (ListSinhvien *i = Lsv.pHead; i != NULL; i = i->pNext) {
				if (XetDK(sv, i->Data) == false) {
					xet = true;
					cout << "\t\t\tMA BAN NHAP BI TRUNG NHA!!! NHAP LAI DI :)"<<endl;
					system("pause");
					break;
				}
			}
			if (xet == 0) {
				ListSinhvien* lsv = KhoiTaoSinhvien(sv);
				ThemVaoCuoiSV(Lsv, lsv);
			}
			
		}
		else if (luachon == 4) {
			cout << "========== Danh sach SinhVien ===========" << endl;
			XuatListSV(Lsv);
			system("pause");
		}
		else if (luachon == 5)
		{
			Khoa k;
			cout << "\nNhap vao ma khoa ban muon in ra thanh danh sach: ";
			while (getchar() != '\n');
			getline(cin, k.Makhoa);
			cout << "Danh sach sinh vien trong khoa " << k.Makhoa << " la: " << endl;
			for (ListSinhvien *sv = Lsv.pHead; sv != NULL; sv = sv->pNext) {
				if (k.Makhoa.compare(sv->Data.Makhoa.Makhoa)==0) {
					Xuat_1_Sinh_Vien(sv->Data);
				}
			}
			system("pause");
		}
		else if (luachon == 6)
		{
			Khoa x;
			cout << "Nhap vao khoa ma ban muon sap xep sinh vien " << endl;
			while (getchar() != '\n');
			getline(cin, x.Makhoa);
			cout << "\nSap xep sinh vien theo tang dan cua khoa: ";
			//SapXepTenSinhVien(Lsv, x.Makhoa);
			sapxepten(Lsv, x.Makhoa);
		}
		else
		{
			break;
		}
	}
}


int main() {
	LIST l;
	list Lsv;
	KhoiTaoDSLKk(l);
	KhoiTaoDSLKSV(Lsv);
	Menu(l, Lsv);
	system("pause");
	return 0;
}