#pragma once
#include <iostream>
#include<vector>
#include<string>
using namespace std;

struct Luong {
	string MaLuong;
	float BacLuong;
	float HeSoLuong;
};

struct NgayThang {
	int Ngay;
	int Thang;
	int Nam;
};
struct Khoa {
	string MaKhoa;
	string TenKhoa;
};

struct NhanVien
{
	string MaNhanVien;
	string Ten;
	string Ho_Dem;
	string GioiTinh;
	string QueQuan;
	NgayThang NgaySinh;
	string TrinhDoChuyenMon;
	string ChucVu;
	NgayThang NgayBatDauCongTac;
	string VienChuc;
	Luong LuongNV;
	Khoa KhoaNV;
};
struct NodeNhanVien {
	NhanVien Data;
	NodeNhanVien* pNext;
};

struct ListNV {
	NodeNhanVien* pHead;
	NodeNhanVien* pTail;
};

struct NodeKhoa {
	Khoa Data;
	ListNV List_Nhan_Vien_Khoa;
	NodeKhoa* pNext;
};

struct ListK {
	NodeKhoa* pHead;
	NodeKhoa* pTail;
};


