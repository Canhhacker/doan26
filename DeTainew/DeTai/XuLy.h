#pragma once
#include "ctdl.h"
#include <string>
#include <fstream>

using namespace std;

NodeNhanVien* KhoiTaoNode(NhanVien x) {
	NodeNhanVien *p = new NodeNhanVien;
	p->Data = x;
	p->pNext = NULL;
	return p;
}

void ThemCuoi(ListNV &l, NodeNhanVien *x) {
	if (l.pHead == NULL)
	{
		l.pHead = l.pTail = x;
	}
	else
	{
		l.pTail->pNext = x;
		l.pTail = x;

	}
}

void KhoiTaoList(ListNV &l) {
	l.pHead = l.pTail = NULL;
}

void Read_1_NV(ifstream &filein, NhanVien &nv)
{
	getline(filein, nv.MaNhanVien, '-');
	getline(filein, nv.Ho_Dem, '-');
	getline(filein, nv.Ten, '-');
	getline(filein, nv.GioiTinh, '-');
	getline(filein, nv.QueQuan, '-');
	getline(filein, nv.TrinhDoChuyenMon, '-');
	getline(filein, nv.ChucVu, '-');
	filein >> nv.NgaySinh.Ngay;
	filein.seekg(1, 1);
	filein >> nv.NgaySinh.Thang;
	filein.seekg(1, 1);
	filein >> nv.NgaySinh.Nam;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Ngay;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Thang;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Nam;
	filein.seekg(1, 1);
	getline(filein, nv.VienChuc, '-');
	getline(filein, nv.KhoaNV.MaKhoa, '-');
	getline(filein, nv.KhoaNV.TenKhoa, '-');
	getline(filein, nv.LuongNV.MaLuong, '-');
	filein >> nv.LuongNV.BacLuong;
	filein.seekg(1, 1);
	filein >> nv.LuongNV.HeSoLuong;

}

void Read_List_NV(ListNV &l) {
	ifstream filein;
	filein.open("NV.txt", ios::in);
	while (!filein.eof())
	{
		NhanVien nv;
		Read_1_NV(filein, nv);
		NodeNhanVien *p = KhoiTaoNode(nv);
		ThemCuoi(l, p);
	}
	filein.close();
}

void Write_1_NV(ofstream &fileout, NhanVien nv) {

	fileout << nv.MaNhanVien << "-";
	fileout << nv.Ho_Dem << "-";
	fileout << nv.Ten << "-";
	fileout << nv.GioiTinh << "-";
	fileout << nv.QueQuan << "-";
	fileout << nv.TrinhDoChuyenMon << "-";
	fileout << nv.ChucVu << "-";
	fileout << nv.NgaySinh.Ngay << "/" << nv.NgaySinh.Thang << "/" << nv.NgaySinh.Nam << "-";
	fileout << nv.NgayBatDauCongTac.Ngay << "/" << nv.NgayBatDauCongTac.Thang << "/" << nv.NgayBatDauCongTac.Nam << "-";
	fileout << nv.VienChuc << "-";
	fileout << nv.KhoaNV.MaKhoa << "-";
	fileout << nv.KhoaNV.TenKhoa << "-";
	fileout << nv.LuongNV.MaLuong << "-";
	fileout << nv.LuongNV.BacLuong << "-";
	fileout << nv.LuongNV.HeSoLuong;

}
void Xuat(NhanVien nv) {
	cout << nv.MaNhanVien << " " << nv.Ho_Dem << " " << nv.Ten << " " << nv.GioiTinh << " " << nv.QueQuan
		<< " " << nv.TrinhDoChuyenMon << " " << nv.ChucVu
		<< " " << nv.NgaySinh.Ngay << "/" << nv.NgaySinh.Thang << "/" << nv.NgaySinh.Nam << " "
		<< nv.NgayBatDauCongTac.Ngay << "/" << nv.NgayBatDauCongTac.Thang << "/" << nv.NgayBatDauCongTac.Nam << "/"
		<< " " << nv.VienChuc << " " << nv.KhoaNV.MaKhoa << " " << nv.KhoaNV.TenKhoa << " " << nv.LuongNV.MaLuong
		<< " " << nv.LuongNV.BacLuong << " " << nv.LuongNV.HeSoLuong;
}
void Xuat_List(ListNV l)
{
	for (NodeNhanVien* i = l.pHead; i != NULL; i = i->pNext)
	{
		Xuat(i->Data);

	}
}
