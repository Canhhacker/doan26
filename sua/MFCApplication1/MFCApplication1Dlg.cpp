﻿
// MFCApplication1Dlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "MFCApplication1.h"
#include "MFCApplication1Dlg.h"
#include "Data_Test.h"
#include "fstream"
#include "file_test.h"
#include "string"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCApplication1Dlg dialog



CMFCApplication1Dlg::CMFCApplication1Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFCAPPLICATION1_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCApplication1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_List, List_ctrl);
	DDX_Control(pDX, IDC_edit_ten, edit_ten);
	DDX_Control(pDX, IDC_edit_ngaysinh, edit_ngaysinh);
	DDX_Control(pDX, IDC_edit_hesoluong, edit_hesoluong);
	DDX_Control(pDX, IDC_edit_gioitinh, edit_gioitinh);
	DDX_Control(pDX, IDC_edit_chucvu, edit_chucvu);
	DDX_Control(pDX, IDC_bnt_motify, bnt_motify);
	DDX_Control(pDX, IDC_edit_hodem, edit_hodem);
	DDX_Control(pDX, IDC_edit_quequan, edit_quequan);
	DDX_Control(pDX, IDC_edit_trinhdo, edit_trinhdo);
	DDX_Control(pDX, IDC_ngaycongtac, edit_ngaycongtac);
	DDX_Control(pDX, IDC_edit_vienchuc, edit_vienchuc);
	DDX_Control(pDX, IDC_makhoa, edit_makhoa);
	DDX_Control(pDX, IDC_tenkhoa, edit_tenkhoa);
	DDX_Control(pDX, IDC_maluong, edit_maluong);
	DDX_Control(pDX, IDC_edit_bacluong, edit_bacluong);
}

BEGIN_MESSAGE_MAP(CMFCApplication1Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_List, OnItemChanged)
	ON_BN_CLICKED(IDC_bnt_motify, &CMFCApplication1Dlg::OnBnClickedbntmotify)
END_MESSAGE_MAP()


// CMFCApplication1Dlg message handlers

BOOL CMFCApplication1Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	List_ctrl.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	List_ctrl.InsertColumn(0, _T("Họ đệm"), LVCFMT_LEFT, 100);
	List_ctrl.InsertColumn(1, _T("Tên"), LVCFMT_LEFT, 50);
	List_ctrl.InsertColumn(2, _T("Giới tính"), LVCFMT_LEFT, 50);
	List_ctrl.InsertColumn(3, _T("Quê quán"), LVCFMT_LEFT, 100);
	List_ctrl.InsertColumn(4, _T("Trình độ"), LVCFMT_LEFT, 50);
	List_ctrl.InsertColumn(5, _T("Chức vụ"), LVCFMT_LEFT, 50);
	List_ctrl.InsertColumn(6, _T("Ngày sinh"), LVCFMT_LEFT, 100);
	List_ctrl.InsertColumn(7, _T("Ngày công tác"), LVCFMT_LEFT, 100);
	List_ctrl.InsertColumn(8, _T("Hợp đồng"), LVCFMT_LEFT, 50);
	List_ctrl.InsertColumn(9, _T("Mã khoa"), LVCFMT_LEFT, 50);
	List_ctrl.InsertColumn(10, _T("Tên khoa"), LVCFMT_LEFT, 100);
	List_ctrl.InsertColumn(11, _T("Mã lương"), LVCFMT_LEFT, 50);
	List_ctrl.InsertColumn(12, _T("Bậc lương"), LVCFMT_LEFT, 50);
	List_ctrl.InsertColumn(13, _T("Hệ số lương"), LVCFMT_LEFT, 50);
	UpdateData(true);
	File file;
	NV n_v;
	ListNV l;
	n_v.KhoiTaoList(l);
	file.Read_List_NV(l);
	for (NodeNhanVien* i = l.pHead; i !=NULL ; i=i->pNext)
	{
		string str_mavn = i->Data.MaNhanVien;
		string str_hodem = i->Data.Ho_Dem;
		string str_ten = i->Data.Ten;
		string str_gioitinh = i->Data.GioiTinh;
		string str_quequan = i->Data.QueQuan;
		string str_ngaysinh = to_string(i->Data.NgaySinh.Ngay) + "/" + to_string(i->Data.NgaySinh.Thang) +"/" + to_string(i->Data.NgaySinh.Nam);
		string str_ngaycongtac= to_string(i->Data.NgayBatDauCongTac.Ngay) + "/" + to_string(i->Data.NgayBatDauCongTac.Thang) +"/"+ to_string(i->Data.NgayBatDauCongTac.Nam);
		string str_vienchuc = i->Data.VienChuc;
		string str_trinhdo = i->Data.TrinhDoChuyenMon;
		string str_chucvu = i->Data.ChucVu;
		string str_makhoa = i->Data.KhoaNV.MaKhoa;
		string str_tenkhoa = i->Data.KhoaNV.TenKhoa;
		string str_maluong = i->Data.LuongNV.MaLuong;
		string str_bacluong = to_string(i->Data.LuongNV.BacLuong);
		string str_hesoluong = to_string(i->Data.LuongNV.HeSoLuong);
		CString cstr_hodem(str_hodem.c_str(), str_hodem.length());
		CString cstr_ten(str_ten.c_str(), str_ten.length());
		CString cstr_gioitinh(str_gioitinh.c_str(), str_gioitinh.length());
		CString cstr_quequan(str_quequan.c_str(), str_quequan.length());
		CString cstr_ngaysinh(str_ngaysinh.c_str(), str_ngaysinh.length());
		CString cstr_ngaycongtac(str_ngaycongtac.c_str(), str_ngaycongtac.length());
		CString cstr_vienchuc(str_vienchuc.c_str(), str_vienchuc.length());
		CString cstr_trinhdo(str_trinhdo.c_str(), str_trinhdo.length());
		CString cstr_makhoa(str_makhoa.c_str(), str_makhoa.length());
		CString cstr_tenkhoa(str_tenkhoa.c_str(), str_tenkhoa.length());
		CString cstr_chucvu(str_chucvu.c_str(), str_chucvu.length());
		CString cstr_maluong(str_maluong.c_str(), str_maluong.length());
		CString cstr_bacluong(str_bacluong.c_str(), str_bacluong.length());
		CString cstr_hesoluong(str_hesoluong.c_str(), str_hesoluong.length());
		List_ctrl.InsertItem(0, cstr_hodem);
		List_ctrl.SetItemText(0, 1, cstr_ten);
		List_ctrl.SetItemText(0, 2, cstr_gioitinh);
		List_ctrl.SetItemText(0, 3, cstr_quequan);
		List_ctrl.SetItemText(0, 4, cstr_trinhdo);
		List_ctrl.SetItemText(0, 5, cstr_chucvu);
		List_ctrl.SetItemText(0, 8, cstr_vienchuc);
		List_ctrl.SetItemText(0, 9, cstr_makhoa);
		List_ctrl.SetItemText(0, 10, cstr_tenkhoa);
		List_ctrl.SetItemText(0, 11, cstr_maluong);
		List_ctrl.SetItemText(0, 12, cstr_bacluong);
		List_ctrl.SetItemText(0, 13, cstr_hesoluong);
		List_ctrl.SetItemText(0, 6, cstr_ngaysinh);
		List_ctrl.SetItemText(0, 7, cstr_ngaycongtac);
	

	}
	
	UpdateData(false);

    


	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMFCApplication1Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMFCApplication1Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMFCApplication1Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CMFCApplication1Dlg::OnItemChanged(NMHDR *pNMHDR, LRESULT *pResult)
{
	NMLISTVIEW* pNMLV = (NMLISTVIEW*)pNMHDR;
	if (pNMLV != NULL)
	{
		index_item_ = pNMLV->iItem;
	}

	if (index_item_ >= 0)
	{
		CString cstr_hodem = List_ctrl.GetItemText(index_item_, 0);
		CString cstr_ten = List_ctrl.GetItemText(index_item_, 1);
		CString cstr_gioitinh = List_ctrl.GetItemText(index_item_, 2);
		CString cstr_quequan = List_ctrl.GetItemText(index_item_, 3);
		CString cstr_trinhdo = List_ctrl.GetItemText(index_item_, 4);
		CString cstr_chucvu = List_ctrl.GetItemText(index_item_, 5);
		CString cstr_ngaysinh = List_ctrl.GetItemText(index_item_, 6);
		CString cstr_ngaycongtac = List_ctrl.GetItemText(index_item_, 7);
		CString cstr_vienchuc = List_ctrl.GetItemText(index_item_, 8);
		CString cstr_makhoa = List_ctrl.GetItemText(index_item_, 9);
		CString cstr_tenkhoa = List_ctrl.GetItemText(index_item_, 10);
		CString cstr_maluong = List_ctrl.GetItemText(index_item_, 11);
		
		CString cstr_hesoluong = List_ctrl.GetItemText(index_item_, 12);
		CString cstr_bacluong = List_ctrl.GetItemText(index_item_, 13);
		
		

		edit_hodem.SetWindowText(cstr_hodem);
		edit_ten.SetWindowText(cstr_ten);
		edit_gioitinh.SetWindowText(cstr_gioitinh);
		edit_quequan.SetWindowText(cstr_quequan);
		edit_trinhdo.SetWindowText(cstr_trinhdo);
		edit_chucvu.SetWindowText(cstr_chucvu);
		edit_ngaysinh.SetWindowText(cstr_ngaysinh);
		edit_ngaycongtac.SetWindowText(cstr_ngaycongtac);
		edit_vienchuc.SetWindowText(cstr_vienchuc);
		edit_makhoa.SetWindowText(cstr_makhoa);
		edit_tenkhoa.SetWindowText(cstr_tenkhoa);
		edit_maluong.SetWindowText(cstr_maluong);
		edit_hesoluong.SetWindowText(cstr_hesoluong);
        edit_bacluong.SetWindowText(cstr_bacluong);
		
	}
}

void CMFCApplication1Dlg::OnBnClickedbntmotify()
{
	CString cstr_ten, cstr_ngaysinh, cstr_gioitinh, cstr_chucvu,cstr_hodem,cstr_quequan,cstr_trinhdo,cstr_ngaycongtac,cstr_vienchuc,cstr_makhoa,cstr_tenkhoa,cstr_maluong,cstr_hesoluong,cstr_bacluong;
	edit_hodem.GetWindowText(cstr_hodem);
	edit_ten.GetWindowText(cstr_ten);
	edit_gioitinh.GetWindowText(cstr_gioitinh);
	edit_quequan.GetWindowText(cstr_quequan);
	edit_trinhdo.GetWindowText(cstr_trinhdo);
	edit_chucvu.GetWindowText(cstr_chucvu);
	edit_ngaysinh.GetWindowText(cstr_ngaysinh);
	edit_ngaycongtac.GetWindowText(cstr_ngaycongtac);
	edit_vienchuc.GetWindowText(cstr_vienchuc);
	edit_makhoa.GetWindowText(cstr_makhoa);
	edit_tenkhoa.GetWindowText(cstr_tenkhoa);
	edit_maluong.GetWindowText(cstr_maluong);
	edit_hesoluong.GetWindowText(cstr_hesoluong);
	edit_bacluong.GetWindowText(cstr_bacluong);
	List_ctrl.SetItemText(index_item_, 0, cstr_hodem);
	List_ctrl.SetItemText(index_item_, 1, cstr_ten);
	List_ctrl.SetItemText(index_item_, 2, cstr_gioitinh);
	List_ctrl.SetItemText(index_item_, 3, cstr_quequan);
	List_ctrl.SetItemText(index_item_, 4, cstr_trinhdo);
	List_ctrl.SetItemText(index_item_, 5, cstr_chucvu);
	List_ctrl.SetItemText(index_item_, 6, cstr_ngaysinh);
	List_ctrl.SetItemText(index_item_, 7, cstr_ngaycongtac);
	List_ctrl.SetItemText(index_item_, 8, cstr_vienchuc);
	List_ctrl.SetItemText(index_item_, 9, cstr_makhoa);
	List_ctrl.SetItemText(index_item_, 10, cstr_tenkhoa);
	List_ctrl.SetItemText(index_item_, 11, cstr_maluong);
	List_ctrl.SetItemText(index_item_, 12, cstr_hesoluong);
	List_ctrl.SetItemText(index_item_, 13, cstr_bacluong);


}
