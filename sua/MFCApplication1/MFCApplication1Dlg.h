
// MFCApplication1Dlg.h : header file
//

#pragma once


// CMFCApplication1Dlg dialog
class CMFCApplication1Dlg : public CDialogEx
{
// Construction
public:
	CMFCApplication1Dlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFCAPPLICATION1_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	CListBox list_ctrl;
public:
	afx_msg void OnItemChanged(NMHDR *pNMHDR, LRESULT *pResult);
	CListCtrl List_ctrl;
	int index_item_;
protected:
	CEdit edit_ten;
	CEdit edit_ngaysinh;
	CEdit edit_hesoluong;
public:
	CEdit edit_gioitinh;
	CEdit edit_chucvu;
	CButton bnt_motify;
	afx_msg void OnBnClickedbntmotify();
	CEdit edit_hodem;
	CEdit edit_quequan;
	CEdit edit_trinhdo;
	CEdit edit_ngaycongtac;
	CEdit edit_vienchuc;
	CEdit edit_makhoa;
	CEdit edit_tenkhoa;
	CEdit edit_maluong;
	CEdit edit_bacluong;
};
