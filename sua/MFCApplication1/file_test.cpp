#include "pch.h"
#include "file_test.h"
#include "pch.h"
#include "file_test.h"
#include "Data_Test.h"
#include <string>
#include <fstream>
using namespace std;
void File::Write_1_NV(ofstream &fileout, NhanVien nv) {

	fileout << nv.MaNhanVien << "-";
	fileout << nv.Ho_Dem << "-";
	fileout << nv.Ten << "-";
	fileout << nv.GioiTinh << "-";
	fileout << nv.QueQuan << "-";
	fileout << nv.TrinhDoChuyenMon << "-";
	fileout << nv.ChucVu << "-";
	fileout << nv.NgaySinh.Ngay << "/" << nv.NgaySinh.Thang << "/" << nv.NgaySinh.Nam << "-";
	fileout << nv.NgayBatDauCongTac.Ngay << "/" << nv.NgayBatDauCongTac.Thang << "/" << nv.NgayBatDauCongTac.Nam << "-";
	fileout << nv.VienChuc << "-";
	fileout << nv.KhoaNV.MaKhoa << "-";
	fileout << nv.KhoaNV.TenKhoa << "-";
	fileout << nv.LuongNV.MaLuong << "-";
	fileout << nv.LuongNV.BacLuong << "-";
	fileout << nv.LuongNV.HeSoLuong;

}

void File::Read_1_NV(ifstream &filein, NhanVien &nv)
{
	getline(filein, nv.MaNhanVien, '-');
	getline(filein, nv.Ho_Dem, '-');
	getline(filein, nv.Ten, '-');
	getline(filein, nv.GioiTinh, '-');
	getline(filein, nv.QueQuan, '-');
	getline(filein, nv.TrinhDoChuyenMon, '-');
	getline(filein, nv.ChucVu, '-');
	filein >> nv.NgaySinh.Ngay;
	filein.seekg(1, 1);
	filein >> nv.NgaySinh.Thang;
	filein.seekg(1, 1);
	filein >> nv.NgaySinh.Nam;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Ngay;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Thang;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Nam;
	filein.seekg(1, 1);
	getline(filein, nv.VienChuc, '-');
	getline(filein, nv.KhoaNV.MaKhoa, '-');
	getline(filein, nv.KhoaNV.TenKhoa, '-');
	getline(filein, nv.LuongNV.MaLuong, '-');
	filein >> nv.LuongNV.BacLuong;
	filein.seekg(1, 1);
	filein >> nv.LuongNV.HeSoLuong;

}

void File::Read_List_NV(ListNV &l) {
	ifstream filein;
	filein.open("text.txt", ios::in);
	while (!filein.eof())
	{
		NhanVien nv;
		Read_1_NV(filein, nv);
		NV a;
		NodeNhanVien *p = a.KhoiTaoNode(nv);
		a.ThemCuoi(l, p);
	}
	filein.close();
}
