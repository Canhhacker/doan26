// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <fstream>
#include <vector>
#include "ctdl.h"
#include <string.h>
using namespace std;



NodeNhanVien* KhoiTaoNode(NhanVien x) {
	NodeNhanVien *p = new NodeNhanVien;
	p->Data = x;
	p->pNext = NULL;
	return p;
}
void ThemCuoi(ListNV &l, NodeNhanVien *x) {
	if (l.pHead == NULL)
	{
		l.pHead = l.pTail = x;
	}
	else
	{
		l.pTail->pNext = x;
		l.pTail = x;

	}
}
void KhoiTaoList(ListNV &l) {
	l.pHead = l.pTail = NULL;
}
NodeKhoa* KhoiTaoNode_K(Khoa x) {
	NodeKhoa *p = new NodeKhoa;
	p->Data = x;
	KhoiTaoList(p->List_Nhan_Vien_Khoa);
	p->pNext = NULL;
	return p;
}
void ThemCuoi_K(ListK &l, NodeKhoa *x) {
	if (l.pHead == NULL)
	{
		l.pHead = l.pTail = x;
	}
	else
	{
		l.pTail->pNext = x;
		l.pTail = x;

	}
}
void KhoiTaoList_K(ListK &l) {
	l.pHead = l.pTail = NULL;
}



void Read_1_NV(ifstream &filein, NhanVien &nv)
{
	getline(filein, nv.MaNhanVien, '-');
	getline(filein, nv.Ho_Dem, '-');
	getline(filein, nv.Ten, '-');
	getline(filein, nv.GioiTinh, '-');
	getline(filein, nv.QueQuan, '-');
	getline(filein, nv.TrinhDoChuyenMon, '-');
	getline(filein, nv.ChucVu, '-');
	filein >> nv.NgaySinh.Ngay;
	filein.seekg(1, 1);
	filein >> nv.NgaySinh.Thang;
	filein.seekg(1, 1);
	filein >> nv.NgaySinh.Nam;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Ngay;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Thang;
	filein.seekg(1, 1);
	filein >> nv.NgayBatDauCongTac.Nam;
	filein.seekg(1, 1);
	getline(filein, nv.VienChuc, '-');
	getline(filein, nv.KhoaNV.MaKhoa, '-');
	getline(filein, nv.KhoaNV.TenKhoa, '-');
	getline(filein, nv.LuongNV.MaLuong, '-');
	filein >> nv.LuongNV.BacLuong;
	filein.seekg(1, 1);
	filein >> nv.LuongNV.HeSoLuong;

}

void Read_List_NV(ListNV &l) {
	ifstream filein;
	filein.open("text.txt", ios::in);
	while (!filein.eof())
	{
		NhanVien nv;
		Read_1_NV(filein, nv);
		NodeNhanVien *p = KhoiTaoNode(nv);
		ThemCuoi(l, p);
	}
	filein.close();
}
void Xuat(NhanVien nv) {
	cout << nv.MaNhanVien << " " << nv.Ho_Dem << " " << nv.Ten << " " << nv.GioiTinh << " " << nv.QueQuan
		<< " " << nv.TrinhDoChuyenMon << " " << nv.ChucVu
		<< " " << nv.NgaySinh.Ngay << "/" << nv.NgaySinh.Thang << "/" << nv.NgaySinh.Nam << " "
		<< nv.NgayBatDauCongTac.Ngay << "/" << nv.NgayBatDauCongTac.Thang << "/" << nv.NgayBatDauCongTac.Nam << "/"
		<< " " << nv.VienChuc << " " << nv.KhoaNV.MaKhoa << " " << nv.KhoaNV.TenKhoa << " " << nv.LuongNV.MaLuong
		<< " " << nv.LuongNV.BacLuong << " " << nv.LuongNV.HeSoLuong;
}
void Xuat_List(ListNV l)
{
	for (NodeNhanVien* i = l.pHead; i != NULL; i = i->pNext)
	{
		Xuat(i->Data);

	}
}

void Read_1_Khoa(ifstream &filein, Khoa &khoa) {
	getline(filein, khoa.MaKhoa, '-');
	getline(filein, khoa.TenKhoa);

}

void Read_List_Khoa(ListK &l) {
	ifstream filein;
	filein.open("Khoa.txt", ios::in);
	while (!filein.eof())
	{
		Khoa a;
		Read_1_Khoa(filein, a);
		NodeKhoa *p = KhoiTaoNode_K(a);
		ThemCuoi_K(l, p);
	}
	filein.close();
	ListNV List_NV;
	KhoiTaoList(List_NV);
	Read_List_NV(List_NV);
	NodeNhanVien *p = List_NV.pHead;
	int flag = 0;
	while (p!=NULL)
	{
		
		NodeNhanVien *temp = p;
		for (NodeKhoa* j = l.pHead; j != NULL&&flag==0; j = j->pNext)
		{
			NodeKhoa *t = j;
			if (p->Data.KhoaNV.MaKhoa == j->Data.MaKhoa)
			{
				ThemCuoi(t->List_Nhan_Vien_Khoa,temp);
				flag = 1;
			}
		}
		p = p->pNext;
	}
		
	
	
}


void Write_1_NV(ofstream &fileout, NhanVien nv) {

	fileout << nv.MaNhanVien << "-";
	fileout << nv.Ho_Dem << "-";
	fileout << nv.Ten << "-";
	fileout << nv.GioiTinh << "-";
	fileout << nv.QueQuan << "-";
	fileout << nv.TrinhDoChuyenMon << "-";
	fileout << nv.ChucVu << "-";
	fileout << nv.NgaySinh.Ngay << "/" << nv.NgaySinh.Thang << "/" << nv.NgaySinh.Nam << "-";
	fileout << nv.NgayBatDauCongTac.Ngay << "/" << nv.NgayBatDauCongTac.Thang << "/" << nv.NgayBatDauCongTac.Nam << "-";
	fileout << nv.VienChuc << "-";
	fileout << nv.KhoaNV.MaKhoa << "-";
	fileout << nv.KhoaNV.TenKhoa << "-";
	fileout << nv.LuongNV.MaLuong << "-";
	fileout << nv.LuongNV.BacLuong << "-";
	fileout << nv.LuongNV.HeSoLuong;

}


int main()
{

	ListK l;
	KhoiTaoList_K(l);
	Read_List_Khoa(l);
	for (NodeKhoa *i = l.pHead; i!=NULL  ; i=i->pNext)
	{
		Xuat_List(i->List_Nhan_Vien_Khoa);
	}
	
	

}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
